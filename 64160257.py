import ipaddress

def subnet_calculator(ip_address, subnet_mask):
    network = ipaddress.IPv4Network(f"{ip_address}/{subnet_mask}", strict=False)

    network_address = network.network_address
    broadcast_address = network.broadcast_address

    return {
        "Network Address": str(network_address),
        "Broadcast Address": str(broadcast_address),
        "Usable Host IP Range": f"{network_address + 1} - {broadcast_address - 1}",
    }

ip_address = input("Enter IP Address: ")
subnet_mask = input("Enter Subnet Mask: ")
result = subnet_calculator(ip_address, subnet_mask)
for key, value in result.items():
    print(f"{key}: {value}")